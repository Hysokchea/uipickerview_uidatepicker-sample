//
//  Constant.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 21/9/22.
//

import Foundation

typealias Completion                = ()                -> Void

enum UserDefaultKey : String {
    case appLang = "appLang"
}
enum NotifyKey : String {
    case reloadLocalize = "reloadLocalize"
}
