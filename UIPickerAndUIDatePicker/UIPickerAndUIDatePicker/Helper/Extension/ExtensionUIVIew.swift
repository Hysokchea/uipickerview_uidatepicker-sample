//
//  ExtensionUIVIew.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 15/9/22.
//

import UIKit
import Foundation

// MARK: - Properties

public extension UIView {
    
    /// Size of view.
    var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.width = newValue.width
            self.height = newValue.height
        }
    }
    
    /// Width of view.
    var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    /// Height of view.
    var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
}

extension UIView {
    
    func superview<T>(of type: T.Type) -> T? {
        return superview as? T ?? superview.flatMap { $0.superview(of: T.self) }
    }
    
}


// MARK: - Methods
public extension UIView {
    
    typealias Configuration = (UIView) -> Swift.Void
    
    func config(configurate: Configuration?) {
        configurate?(self)
    }
    
    /// Set some or all corners radiuses of view.
    ///
    /// - Parameters:
    ///   - corners: array of corners to change (example: [.bottomLeft, .topRight]).
    ///   - radius: radius for selected corners.
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
}

extension UIView {
    
    func searchVisualEffectsSubview() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let found = subview.searchVisualEffectsSubview() {
                    return found
                }
            }
        }
        return nil
    }
    
    /// This is the function to get subViews of a view of a particular type
    /// https://stackoverflow.com/a/45297466/5321670
    func subViews<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T{
                all.append(aView)
            }
        }
        return all
    }
    
    
    /// This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T
    /// https://stackoverflow.com/a/45297466/5321670
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
}
 
 extension UIView {
     func makeClearViewWithShadow(
         cornderRadius: CGFloat,
         shadowColor: CGColor,
         shadowOpacity: Float,
         shadowRadius: CGFloat) {

         self.frame = self.frame.insetBy(dx: -shadowRadius * 2,
                                         dy: -shadowRadius * 2)
         self.backgroundColor = .clear
         let shadowView = UIView(frame: CGRect(
             x: shadowRadius * 2,
             y: shadowRadius * 2,
             width: self.frame.width - shadowRadius * 4,
             height: self.frame.height - shadowRadius * 4))
         shadowView.backgroundColor = .black
         shadowView.layer.cornerRadius = cornderRadius
         shadowView.layer.borderWidth = 1.0
         shadowView.layer.borderColor = UIColor.clear.cgColor

         shadowView.layer.shadowColor = shadowColor
         shadowView.layer.shadowOpacity = shadowOpacity
         shadowView.layer.shadowRadius = shadowRadius
         shadowView.layer.masksToBounds = false
         self.addSubview(shadowView)

         let p:CGMutablePath = CGMutablePath()
         p.addRect(self.bounds)
         p.addPath(UIBezierPath(roundedRect: shadowView.frame, cornerRadius: shadowView.layer.cornerRadius).cgPath)

         let s = CAShapeLayer()
         s.path = p
         s.fillRule = CAShapeLayerFillRule.evenOdd

        self.layer.mask = s
     }
 }
