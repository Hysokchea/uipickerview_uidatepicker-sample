//
//  UIPickerPopUpVC.swift
//  UIPickerAndUIDatePicker
//
//  Created by Huort Seanghay on 4/10/22.
//

import UIKit

class UIPickerPopUpVC: UIViewController {

    @IBOutlet weak var PickerBtn: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    let screenWidth         = UIScreen.main.bounds.width - 10
    let screenHeight        = UIScreen.main.bounds.height / 2
    var selectRow           = 0
    var selectRowTextColor  = 0
    
    var Club: KeyValuePairs = [
        "Chelsea FC"            : UIImage(named: "Chelsea_fc"),
        "Manchester United FC"  : UIImage(named: "ManUnited"),
        "Manchester City FC"    : UIImage(named: "ManCity"),
        "Liverpool FC"          : UIImage(named: "Liverpool"),
        "Arsenal FC"            : UIImage(named: "Arsenal")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func didTapPickerBtnAction(_ sender: Any) {
        let vc           = UIViewController()
        let pickerView  = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        vc.preferredContentSize = CGSize(width: screenWidth, height: screenHeight)
        vc.view.addSubview(pickerView)
        pickerView.dataSource   = self
        pickerView.delegate     = self
        pickerView.selectRow(selectRow, inComponent: 0, animated: false)
        pickerView.centerXAnchor.constraint(equalTo: vc.view.centerXAnchor).isActive = true
        
        let alert = UIAlertController(title: "Select Your Club", message: "", preferredStyle: .actionSheet)
        
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction) in
        }))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            self.selectRow          = pickerView.selectedRow(inComponent: 0)
            let selected            = Array(self.Club)[self.selectRow]
            let colors              = selected.value
            let name                = selected.key

            self.imageView.image = colors
            self.PickerBtn.setTitle(name, for: .normal)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func didTapSubmitAction(_ sender: Any) {
        self.customToastwithColor(image: "Done", message: "Your vote has been submit successfully!", backgroundColor: .tintColor)
    }
}

extension UIPickerPopUpVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    // Asks the data source for the number of components in the picker view.
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // Asks the data source for the number of rows for a specified component.
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return Club.count
    }
    
    // Called by the picker view when it needs the row width to use for drawing row content.
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        250
    }
    
    // Called by the picker view when it needs the row height to use for drawing row content.
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    // Called by the picker view when it needs the view to use for a given row in a given component.
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {

        var pickerLabel : UILabel
        if let label = view as? UILabel {
            pickerLabel = label
        } else {
            pickerLabel                 = UILabel()
            pickerLabel.textColor       = UIColor.black
            pickerLabel.textAlignment   = NSTextAlignment.center
        }

        pickerLabel.text            = Array(Club)[row].key
        pickerLabel.font            = UIFont(name: "Rubik-Medium", size: 30)
//        pickerLabel.backgroundColor = .orange
        pickerLabel.textColor       = .red
        pickerLabel.sizeToFit()

        return pickerLabel
    }
    
    // Called by the picker view when it needs the title to use for a given row in a given component.
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        let data = Array(pickerData)[row].key
//        self.dob = data
//        return data
//    }
    
    // Called by the picker view when it needs the styled title to use for a given row in a given component.
//    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
//        let data = Array(Club)[row].key
//        let myTitle = NSAttributedString(string     : data,
//                                         attributes : [NSAttributedString.Key.foregroundColor: UIColor.link, NSAttributedString.Key.backgroundColor: UIColor.lightGray])
//
//        return myTitle
//    }
    
    // Called by the picker view when the user selects a row in a component.
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        self.selectRow          = self.picker.selectedRow(inComponent: 0)
//        let selected            = Array(self.pickerData)[self.selectRow]
//        selectRow               = row
//
//    }
}
