//
//  BookListContentVC.swift
//  UIPickerAndUIDatePicker
//
//  Created by Huort Seanghay on 4/10/22.
//

import UIKit

class BookListContentVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func didTapPickerPopUpAction(_ sender: Any) {
        self.pushViewController(sbName: "UIPickerPopUp", identifier: "UIPickerPopUpVC")
    }
    
    @IBAction func didTapDatePickerAndPickerAction(_ sender: Any) {
        self.pushViewController(sbName: "SignUpSB", identifier: "SignUpVC")
    }

}
