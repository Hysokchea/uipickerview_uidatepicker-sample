//
//  SignUpModel.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 16/9/22.
//

import Foundation

// type of each cell
enum InputType{
    case PickDOB
    case PickRegion
}

struct SignUpModel  : ObjSignUpModel {
    var title       : String?
    var value       : String?
    var placeHolder : String?
    var type        : InputType?
}

protocol ObjSignUpModel {
    var title       : String? { get set}
    var value       : String? { get set}
    var placeHolder : String? { get set}
    var type        : InputType? { get set}
}
