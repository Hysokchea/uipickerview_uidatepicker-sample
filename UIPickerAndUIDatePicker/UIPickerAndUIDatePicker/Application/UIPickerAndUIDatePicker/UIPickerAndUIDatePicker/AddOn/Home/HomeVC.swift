//
//  HomeVC.swift
//  UIPickerAndUIDatePicker
//
//  Created by Huort Seanghay on 4/10/22.
//

import UIKit

class HomeVC: UIViewController {

    //MARK: - IBOutlet
    
    //UIView
    @IBOutlet weak var profileView: UIView!
    
    //UIButton
    @IBOutlet weak var YTBtn: UIButton!
    @IBOutlet weak var IGBtn: UIButton!
    @IBOutlet weak var LKBtn: UIButton!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.profileView.addBorder()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBAction
    
    @IBAction func didTapIGBtnAction(_ sender: Any) {
        self.callWeb(url: "https://www.instagram.com/?hl=en")
    }
    
    @IBAction func didTapYTBtnAction(_ sender: Any) {
        self.callWeb(url: "https://www.youtube.com/channel/UC-u9emd2ZWBnhaE2dgdkNMQ/videos")
    }
    
    @IBAction func didTapLKBtnAction(_ sender: Any) {
        self.callWeb(url: "https://www.linkedin.com/in/seanghay-huort-745683214/")
    }
    
    // Func for push to webView
    func callWeb(url: String?) {
        let webKit = self.VC(sbName: "webSB", identifier: "WebVC") as! WebVC
        webKit.hidesBottomBarWhenPushed = true
        webKit.urlStr   = url ?? ""
        self.navigationController?.pushViewController(webKit, animated: true)
    }
    
    func VC(sbName: String, identifier: String) -> UIViewController {
        return UIStoryboard(name: sbName, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }

}
