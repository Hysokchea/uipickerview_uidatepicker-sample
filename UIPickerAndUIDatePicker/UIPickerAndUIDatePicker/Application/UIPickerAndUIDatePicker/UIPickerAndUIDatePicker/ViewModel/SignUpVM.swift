//
//  SignUpVM.swift
//  KosignBooks
//
//  Created by Huort Seanghay on 16/9/22.
//

import Foundation

class SignUpVM {
    
    var cells  = [ObjSignUpModel]()
    
    func setSignUpData() {
        
        cells = [
            SignUpModel(title: "Choose your Date of Birth",
                        value: "",
                        placeHolder: "Select Date of Birth",
                        type: .PickDOB),
            SignUpModel(title: "Please Input Name",
                        value: "",
                        placeHolder: "Select your Region",
                        type: .PickRegion),
        ]
    }
}
